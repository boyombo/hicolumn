import React from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import SubCategoryButton from "./SubCategoryButton";

export default class SubCategoryBar extends React.Component {
  render() {
    console.log(this.props.items);
    console.log("inside sub-category");
    return (
      <View style={styles.container}>
        {this.props.items.map((item, idx) => (
          <SubCategoryButton title={item.name} key={idx} />
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  }
});
