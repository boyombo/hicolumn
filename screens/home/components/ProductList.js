import React from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { Card, Text } from "react-native-elements";
import ProductView from "./ProductView";
import { white, light } from "../../../constants/Colors";

export default class ProductList extends React.Component {
  _keyExtractor = (item, idx) => idx;

  _renderItem = ({ item }) => <ProductView item={item} />;

  render() {
    console.log("====================================");
    console.log("PRODUCT LIST");
    console.log(this.props.items);
    console.log("====================================");
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.items}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          numColumns={3}
          style={{
            marginBottom: 120,
            backgroundColor: light,
            padding: 0
            // flex: 1
          }}
          contentContainerStyle={{
            // flex: 1,
            // marginBottom: 120,
            // padding: 0,
            // alignContent: "stretch",
            // justifyContent: "flex-start",
            marginHorizontal: 1,
            alignItems: "center"
            // backgroundColor: "#ccc"
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: "#ccc"
    // alignSelf: "stretch"
  }
});
