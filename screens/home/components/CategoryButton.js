import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { Avatar } from "react-native-elements";
import { medium, white } from "../../../constants/Colors";
import { Icon } from "react-native-elements";

export default class CategoryButton extends React.Component {
  render() {
    const icon = this.props.icon || "person";
    const iconType = this.props.iconType || "material-community";
    const iconColor = this.props.iconColor || medium;
    const bgColor = this.props.bgColor || white;
    return (
      <View style={styles.container}>
        <View style={styles.avatar}>
          <Icon name={icon} type={iconType} color={iconColor} />
        </View>
        {/* <Avatar
          rounded
          size="medium"
          icon={{ name: icon, color: iconColor, type: iconType }}
          containerStyle={styles.avatar}
          overlayContainerStyle={{ backgroundColor: bgColor }}
          //   onPress={this.props.onPress}
        /> */}
        <Text style={styles.text}>{this.props.name}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    marginLeft: 5
    // backgroundColor: "black"
  },
  avatar: {
    borderColor: medium,
    // borderColor: '#004e98',
    borderWidth: 1,
    padding: 10,
    borderRadius: 50
  },
  icon: {
    backgroundColor: medium
    // backgroundColor: '#004e98'
  },
  text: {
    fontSize: 9,
    fontWeight: "bold"
    // flex: 1,
    // flexWrap: "wrap"
  }
});
