import React from "react";
import { View, StyleSheet } from "react-native";
import { Text, Button } from "react-native-elements";
import { medium } from "../../../constants/Colors";

export default class SubCategoryButton extends React.Component {
  render() {
    return (
      <Button
        title={this.props.title}
        type="outline"
        titleStyle={{ color: medium, fontSize: 10 }}
        buttonStyle={{ borderColor: medium, margin: 2 }}
      />
    );
  }
}
