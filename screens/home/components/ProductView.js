import React from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Card, Text } from "react-native-elements";
import { light, medium, white } from "../../../constants/Colors";

class ProductView extends React.Component {
  render() {
    return (
      // <Card
      //   title={this.props.item.title}
      //   style={styles.container}
      //   titleStyle={styles.title}
      //   featuredSubtitle="Test"
      // >
      <TouchableOpacity style={styles.card}>
        {/* <Image
          style={styles.image}
          source={require("../../../assets/images/kellogs.jpg")}
          resizeMode="cover"
        /> */}
      </TouchableOpacity>

      // </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: medium,
    padding: 0,
    margin: 5,
    borderRadius: 10,
    borderColor: "black",
    borderWidth: 1
  },
  title: {
    fontSize: 10
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
    // width: 100,
    // height: 120,
    backgroundColor: white
  },
  card: {
    margin: 2,
    padding: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: medium,
    // width: "100%"
    width: 120,
    height: 150
    // backgroundColor: light
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
    //overlay efect
    flex: 1,
    height: 200,
    width: null,
    position: "absolute",
    zIndex: 100,
    left: 0,
    right: 0,
    backgroundColor: "transparent"
  },
  cardFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 15,
    paddingBottom: 0,
    paddingVertical: 7.5,
    paddingHorizontal: 0
  },
  cardImage: {
    flex: 1,
    height: 150,
    width: null
  }
});
export default ProductView;
