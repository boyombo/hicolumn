import React from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import { ButtonGroup, Card } from "react-native-elements";
import {
  white,
  medium,
  light,
  dark,
  vlight,
  gray,
  lightGray
} from "../../constants/Colors";
import { CategoryButton, SubCategoryBar, ProductList } from "./components";
import { categories, subCategories, productList } from "./data";

class HomeScreen extends React.Component {
  state = {
    selectedCategory: 0
  };
  componentDidMount() {
    console.log("inside home");
  }
  render() {
    console.log(categories);
    // const buttons = buttonTitles.map(item => <CategoryButton title={item} />);
    console.log("running");
    return (
      <View style={styles.contentContainer}>
        <View
          style={{
            flexDirection: "row",
            marginTop: 10,
            justifyContent: "space-evenly"
          }}
        >
          {categories.map((item, idx) => (
            <CategoryButton
              key={idx}
              name={item.name}
              icon={item.icon}
              iconType={item.iconType}
            />
          ))}
        </View>
        <View
          horizontal
          style={{
            borderColor: "red",
            borderTopColor: medium,
            borderBottomColor: medium,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            padding: 5,
            margin: 5
          }}
        >
          <ScrollView horizontal>
            <SubCategoryBar items={subCategories} />
          </ScrollView>
        </View>
        <View style={{ margin: 5 }}>
          <ProductList items={productList} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: white,
    alignContent: "center",
    justifyContent: "flex-start"
  }
});
export default HomeScreen;
