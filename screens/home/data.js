export const categories = [
  { name: "Fast Foods", icon: "food", iconType: "material-community" },
  { name: "Groceries", icon: "shopping-basket", iconType: "material" },
  { name: "Drinks", icon: "glass-wine", iconType: "material-community" },
  { name: "Baby Food", icon: "baby", iconType: "material-community" },
  {
    name: "Fresh Foods",
    icon: "food-apple",
    iconType: "material-community"
  },
  {
    name: "Toileteries",
    icon: "spray-bottle",
    iconType: "material-community"
  }
];

export const subCategories = [
  { name: "Sub1" },
  { name: "Sub2" },
  { name: "Sub3" },
  { name: "Sub4" },
  { name: "Sub5" },
  { name: "Sub6" },
  { name: "Sub7" },
  { name: "Sub8" },
  { name: "Sub9" },
  { name: "Sub10" },
  { name: "Sub11" },
  { name: "Sub12" },
  { name: "Sub13" },
  { name: "Sub14" },
  { name: "Sub15" },
  { name: "Sub16" }
];

export const productList = [
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  },
  {
    title: "Corn Flakes",
    uri:
      "https://images-na.ssl-images-amazon.com/images/I/71Qe1%2B-nrkL._SL1500_.jpg"
  }
];
