const tintColor = "#DC042A";
// const tintColor = "#2f95dc";

export default {
  tintColor,
  tabIconDefault: "#ccc",
  tabIconSelected: tintColor,
  // tabBar: "#fefefe",
  tabBar: "#FCFCFC",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff"
};

export const medium = "#DC042A";
export const light = "#D2918B";
export const dark = "#AC1D1A";
export const vlight = "#fffeff";
export const white = "#ffffff";
export const gray = "#808080";
export const lightGray = "#CCC";
