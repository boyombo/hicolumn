import React from "react";
import { Platform } from "react-native";
import {
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/home/HomeScreen";
import { medium, white } from "../constants/Colors";

const defaultOptions = {
  defaultNavigationOptions: {
    title: "Hi Column",
    headerStyle: {
      backgroundColor: medium
    },
    headerTintColor: white,
    headerTintStyle: {
      fontWeight: "bold"
    }
  }
};

const ShopStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  defaultOptions
);

ShopStack.navigationOptions = {
  tabBarLabel: "Shops",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="store" type="material" />
  )
};

const DeviceStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  defaultOptions
);

DeviceStack.navigationOptions = {
  tabBarLabel: "Devices",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="mobile" type="font-awesome" />
  )
};

const WearStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  defaultOptions
);

WearStack.navigationOptions = {
  tabBarLabel: "Wears",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name="tshirt-crew"
      type="material-community"
    />
  )
};

const FittingStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  defaultOptions
);

FittingStack.navigationOptions = {
  tabBarLabel: "Home & Fittings",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name="lightbulb-outline" type="material" />
  )
};

export default createBottomTabNavigator({
  ShopStack,
  DeviceStack,
  WearStack,
  FittingStack
});
