import React from "react";
import { createSwitchNavigator, createAppContainer } from "react-navigation";

import MainTabNavigator from "./MainTabNavigator";

const AppNavigator = createSwitchNavigator(
  {
    Main: MainTabNavigator
  },
  { initialRouteName: "Main" }
);

export default createAppContainer(AppNavigator);
